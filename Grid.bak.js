var Grid = (function () {
    console.log('123');
    // 은닉될 멤버 정의
    // 테이블을 그리는 요소는 모두 이곳에 배치한다.

    var gridConf;
    var colConf;

    // 생성자
    var Grid = function (opt) {
        gridConf = new GridConfig(opt);
        console.log(gridConf.elementId);
        colConf = new ColumnConfig(gridConf);
        init();

        // 공개될 멤버 정의 this 사용
        this.addRow = function () {
            console.log('ab');
            addRow(colConf);
        }
        this.removeRow = function (idx) {
            removeRow(idx);
        }

        this.getRowIndex = function (el) {
            return getRowIndex(el);
        }

        this.getRowData = function (idx) {
            return getRowData(idx);
        }

        this.getAllRowData = function () {
            return getAllRowData();
        }
    }

    function init() {
        // Table 공간 생성
        makeGridDiv(gridConf, colConf);

        // Table 생성
        drawGrid(gridConf, colConf);

        // 스크롤 이벤트 적용
        setScrollEventListener(gridConf.elementId, colConf.hasFixedColumn);
    }

    function setScrollEventListener(elId, hasFixedCol) {
        /// 스크롤 동기화
        if (hasFixedCol) {
            var master = elId + "_tbl_general_row_Div"; // this is id div
            var slave = elId + "_tbl_fixed_row_Div"; // this is other id div
            var master_tmp;
            var slave_tmp;
            var timer;

            var sync = function () {
                if ($(this).attr('id') == slave) {
                    master_tmp = master;
                    slave_tmp = slave;
                    master = slave;
                    slave = master_tmp;
                }

                $("#" + slave).unbind("scroll");
                var percentage = this.scrollTop / (this.scrollHeight - this.offsetHeight);
                var x = percentage * ($("#" + slave).get(0).scrollHeight - $("#" + slave).get(0).offsetHeight);
                $("#" + slave).scrollTop(x);
                if (typeof (timer) !== 'undefind')
                    clearTimeout(timer);
                timer = setTimeout(function () { $("#" + slave).scroll(sync) }, 200)
            }
            $('#' + master + ', #' + slave).scroll(sync);
        }

        $('#' + elId + '_tbl_general_row_Div').scroll(function (e) {
            console.log('a');
            // 동기화
            $('#' + elId + '_tbl_general_header_Div').scrollLeft($('#' + elId + '_tbl_general_row_Div').scrollLeft());
        });

    }

    // Grid 환경 변수
    function GridConfig(opt) {
        this.elementId = opt.grid.elementId;
        this.grid = opt.grid;
        this.width = opt.grid.width;
        this.useScrollbar = opt.grid.useScrollbar;
        this.height = opt.grid.height;
        this.Header = opt.header;
        this.columnList = opt.columnList;
        this.Toolbar = opt.toolbar;
        this.Pagination = opt.pagination;
        this.data = opt.data;
        this.widthRatio = 1;

        if (!opt.grid.hasOwnProperty('width')) {
            this.width = $('#' + this.elementId).width();
            console.log('this.width = ' + this.width);
        }

        // 사용 여부 판단
        this.useHeader = false;
        this.useToolbar = false;
        this.usePagination = false;

        if (opt.hasOwnProperty('header')) this.useHeader = true;
        if (opt.hasOwnProperty('toolbar')) this.useToolbar = true;
        if (opt.hasOwnProperty('pagination')) this.usePagination = true;

    }

    // Column 환경 변수
    function ColumnConfig(gridConf) {
        var columns = gridConf.columnList;
        this.hasFixedColumn = false;
        this.fixedColumn = new Array();
        this.fixedTblWidth = 0;
        this.generalColumn = new Array();
        this.generalTblWidth = 0;

        for (var i = 0; i < columns.length; i++) {
            if (columns[i].hasOwnProperty('fixed') && columns[i].fixed) {
                this.hasFixedColumn = true;
                this.fixedColumn.push(columns[i]);
                this.fixedTblWidth += columns[i].width;
            } else {
                this.generalTblWidth += columns[i].width;
                this.generalColumn.push(columns[i]);
            }
        }

        // 넓이에 대한 비율을 구해보자.
        var widthRatio = (this.fixedTblWidth + this.generalTblWidth) / gridConf.width;
        gridConf.widthRatio = widthRatio;

        this.fixedTblWidth = this.fixedTblWidth / widthRatio;
        this.generalTblWidth = this.generalTblWidth / widthRatio;
    }

    /**
     * makeGridDiv
     * 그리드를 적용할 공간을 생성한다.
     * 고정 열 여부에 따라 다르다.
     */
    function makeGridDiv() {
        // scroll 사용을 위해 테이블과 같은 크기로 맞춰야 한다.
        var elId = gridConf.elementId;
        if (colConf.hasFixedColumn) {
            var fixedDiv = $('<div/>', {id: elId + '_L', style: 'width:' + colConf.fixedTblWidth + 'px; display:inline-block;'});
            $('#' + elId).append(fixedDiv);
        }
        var generalDiv = $('<div/>', {id: elId + '_R',style: 'width:' + colConf.generalTblWidth + 'px; display:inline-block;'});
        $('#' + elId).append(generalDiv);
    }

    /**
     * drawGrid
     * Header의 최대 행수를 구하여 그리드를 그린다.
     */
    function drawGrid() {
        var maxHeaderRowCnt = getMaxHeaderRowCnt(gridConf);
        var elementId = gridConf.elementId;
        make(maxHeaderRowCnt);

        // 그리드를 생성한 후 최상위 Div의 Width 변경
        //$('#' + elementId).css('width', $('#' + elementId + '_R').width() + 4 + $('#' + elementId + '_L').width() + 4);
    }

    /**
     * make
     * 그리드를 그린다.
     * @param {GridConfig} gridConf 
     * @param {ColumnConfig} colConf 
     * @param {number} maxHeaderRowCnt 
     */
    function make(maxHeaderRowCnt) {
        var elId = gridConf.elementId;

        var fixedColumn = colConf.fixedColumn;
        var fixedTblWidth = Number(colConf.fixedTblWidth);
        var generalColumn = colConf.generalColumn;
        var generalTblWidth = Number(colConf.generalTblWidth);
        var header = gridConf.Header;
        var data = gridConf.data;
        var toolbar = gridConf.Toolbar;

        console.log('generalTblWidth = ' + generalTblWidth);

        //툴바 먼저 그림니다.
        makeToolbar(elId, toolbar, gridConf.width);

        if (colConf.hasFixedColumn) {
            // Fixed Column Table
            makeTable(elId + '_L', elId + '_tbl_fixed_header', fixedTblWidth);
            makeHeader(elId + '_tbl_fixed_header', fixedColumn, header, maxHeaderRowCnt);
            makeRow(elId + '_L', elId + '_tbl_fixed_row', fixedColumn, data, fixedTblWidth);
        }

        // General Column Table
        makeTable(elId + '_R', elId + '_tbl_general_header', generalTblWidth);
        makeHeader(elId + '_tbl_general_header', generalColumn, header, maxHeaderRowCnt);
        makeRow(elId + '_R', elId + '_tbl_general_row', generalColumn, data, generalTblWidth);

        // Pagination
        if (gridConf.usePagination) makePagination(elId, 10, 45, 2);
    }

    /**
     * makePagination
     * 페이징 설정
     * @param {string} elementId 
     * @param {number} pageCnt 
     * @param {number} maxPageCnt 
     * @param {number} currentPage 
     */
    function makePagination(elementId, pageCnt, maxPageCnt, currentPage) {
        var pDiv = document.createElement('div');
        pDiv.className = 'paginationDiv';

        var div = document.createElement('div');
        div.className = 'pagination';

        var grpIdx = Math.floor((currentPage - 1) / pageCnt);
        var grpMaxPageCnt = (grpIdx + 1) * 10;
        if (grpMaxPageCnt > maxPageCnt) {
            grpMaxPageCnt = maxPageCnt;
        }

        // prev page
        if (currentPage > pageCnt) {
            makeATag(div, '«', '');
        }

        // page
        for (var i = grpIdx * pageCnt + 1; i <= grpMaxPageCnt; i++) {
            var className = '';
            if (i == currentPage) className = 'active';
            makeATag(div, i, className);
        }

        // next page
        if (pageCnt <= maxPageCnt) {
            makeATag(div, '»', '');
        }

        pDiv.appendChild(div);
        $('#' + elementId).append(pDiv);
    }

    /**
     * makeATag
     * a Tag 생성
     * @param {object} div 
     * @param {string} value 
     * @param {string} className 
     */
    function makeATag(div, value, className) {
        var a = document.createElement('a');
        a.setAttribute('href', '#');
        if (className != '') {
            a.className = 'active';
        }
        a.innerHTML = value;
        div.appendChild(a);
    }

    /**
     * Toolbar 생성
     * @param {string} elementId 
     * @param {Toolbar} toolbar 
     * @param {number} width 
     */
    function makeToolbar(elementId, toolbar, width) {
        if (toolbar === undefined) return;
        $('#' + elementId).prepend($('<div/>', {id: elementId + '_Tool',style: 'width:' + width + 'px;'}));
        $('#' + elementId + '_Tool').append($('<div/>', {id: elementId + '_Tools',style: 'float:right;'}));

        for (var i = 0; i < toolbar.length; i++) {
            var aTag = $('<a/>').attr('name', toolbar[i].name).attr('href', '#').text(toolbar[i].title).addClass('toolbarItem');
            /*
            var aTag = document.createElement('a');
            aTag.innerHTML = toolbar[i].title;
            aTag.setAttribute('name', toolbar[i].name);
            aTag.setAttribute('href', '#');
            aTag.className = 'toolbarItem';
            */
            for (var j = 0; j < Object.keys(toolbar[i].event).length; j++) {
                //aTag.addEventListener(Object.keys(toolbar[i].event)[j], toolbar[i].event[Object.keys(toolbar[i].event)[j]]);
                aTag.bind(Object.keys(toolbar[i].event)[j], toolbar[i].event[Object.keys(toolbar[i].event)[j]]);
            }
            $('#' + elementId + '_Tools').append(aTag);
        }
    }

    /**
     * makeTable
     * 테이블 생성
     * 고정 열과 일반 열의 Div를 구분지어 테이블 생성
     * @param {string} elId 
     * @param {string} tblNm 
     * @param {number} width 
     */
    function makeTable(elId, tblNm, width) {
        // 1. 그리드를 그릴 element에 Table을 생성 한다.
        // 각각의 테이블을 감싸는 DIV 도 생성한다.
        //gridConf.useScrollbar
        if (tblNm.indexOf('header') != -1) { // 헤더
            if (tblNm.indexOf('fixed') != -1) { // 고정열
                $('#' + elId).append('<div id="' + tblNm + '_Div" style="display:inline-block; width:' + width + 'px; overflow-x:hidden;"></div>')
            } else {
                $('#' + elId).append('<div id="' + tblNm + '_Div" style="display:inline-block; width:' + width + 'px; overflow-x:hidden; overflow-y:scroll;"></div>')
            }
        } else {
            if (tblNm.indexOf('fixed') != -1) {
                $('#' + elId).append('<div id="' + tblNm + '_Div" style="display:inline-block;height:200px; width:' + width + 'px; overflow-x:scroll; overflow-y:scroll;"></div>')
            } else {
                $('#' + elId).append('<div id="' + tblNm + '_Div" style="display:inline-block;height:200px; width:' + width + 'px; overflow-x:scroll; overflow-y:scroll;"></div>')
            }
        }

        $('#' + tblNm + '_Div').append('<table border="1" name="' + tblNm + '" style="width:' + (width - 20) + 'px; "></table>');
    }

    /**
     * makeColGrp
     * colgroup 생성
     * @param {string} tblNm 
     * @param {columnList} columnList 
     */
    function makeColGrp(tblNm, columnList) {
        console.log('tblNm = ' + tblNm);
        var colGrp = '<colgroup>';
        var thHtml = '';
        for (var i = 0; i < columnList.length; i++) {
            var style = '';
            if (columnList[i].hasOwnProperty('width') && gridConf.useScrollbar) {
                style = 'style="width : ' + columnList[i].width + 'px;"';
            } else {
                if (gridConf.widthRatio <= 1) {
                    style = 'style="width : ' + columnList[i].width * gridConf.widthRatio + 'px;"';
                } else {
                    style = 'style="width : ' + columnList[i].width / gridConf.widthRatio + 'px;"';
                }
            }
            colGrp += '<col colNm="col_' + columnList[i].name + '" ' + style + '>';
        }
        colGrp += '</colgroup>'

        $('[name=' + tblNm + ']').append(colGrp);
    }

    /**
     * makeHeader
     * table의 header 생성
     * @param {string} tblNm 
     * @param {columnlist} columnList 
     * @param {Headers} headerOpt 
     * @param {number} maxHeaderRowCnt 
     */
    function makeHeader(tblNm, columnList, headerOpt, maxHeaderRowCnt) {
        makeColGrp(tblNm, columnList);
        var thHtml = '';

        // 헤더 머지 여부 체크 - 머지 옵션이 있는지 확인한다
        if (headerOpt.hasOwnProperty('merge')) {
            thHtml += mergeHeader(columnList, headerOpt, maxHeaderRowCnt);
        } else {
            for (var i = 0; i < columnList.length; i++) {
                //    if (isFixed && columnList[i].hasOwnProperty('fixed')) continue;
                thHtml += '<th style="height:' + headerOpt.height + 'px">' + columnList[i].title + '</th>';
            }
        }
        $('[name=' + tblNm + ']').append('<tbody>' + thHtml + '</tbody>');
    }

    /**
     * makeRow
     * 데이터를 출력할 Row, Cell 생성
     * @param {string} elId 
     * @param {string} tblNm 
     * @param {string} column 
     * @param {Data} data 
     * @param {number} width 
     */
    function makeRow(elId, tblNm, column, data, width) {
        // 헤더와 데이터로우는 각각의 테이블을 사용한다.
        makeTable(elId, tblNm, width);
        makeColGrp(tblNm, column);
        var tdHtml = '';

        for (var i = 0; i < data.length; i++) {
            // tdHtml += '<tr>';
            var tr = document.createElement('tr');
            tr.setAttribute('data_row', i + 1);
            for (var j = 0; j < column.length; j++) {
                var cellTypeHtml = '';
                var dataValue = '';
                var td = '';
                if (column[j].hasOwnProperty('values')) {
                    // values 옵션을 사용한다는 것은 여러 값을 같이 사용한다는 것
                    dataValue = new Array();
                    // j 값을 신경쓰지말고 그냥 직접 가져온다
                    for (var k = 0; k < column[j].values.length; k++) {
                        dataValue.push(data[i][column[j].values[k]]);
                    }
                    td = setCellData(dataValue, column[j]);
                } else {
                    dataValue = data[i][column[j].name];
                    //    tdHtml += setCellData(dataValue, column[j]);
                    td = setCellData(dataValue, column[j]);
                }

                var className = (i % 2 == 0) ? 'row1' : 'row2';
                td.className = className;
                tr.append(td);
            }
            $('[name=' + tblNm).append(tr);
        }
        dataRowspan(column);
    }

    function setCellInput(value, columnOption) {

        var tdTag = document.createElement('td');
        if (columnOption.hasOwnProperty('inputOption')) {
            var span = document.createElement('span');
            var input = document.createElement('input');
            var keys = Object.keys(columnOption.inputOption);
            for (var i = 0; i < keys.length; i++) {
                if (keys[i] == 'event') {
                    var eventKeys = Object.keys(columnOption.inputOption.event);
                    for (var j = 0; j < eventKeys.length; j++) {
                        var evt = columnOption.inputOption.event[eventKeys[j]];
                        input.addEventListener(eventKeys[j], evt);
                    }
                } else {
                    input.setAttribute(keys[i], columnOption.inputOption[keys[i]] || value);
                }
            }
            tdTag.appendChild(input);
            tdTag.appendChild(span);
        }
        if (columnOption.hasOwnProperty('editOption')) {
            var span = $(this).find('span');
            span.hide();

            if (!columnOption.editOption.hasOwnProperty('options')) {

                var editType = columnOption.editOption.type;
                if (editType == 'selectbox') {
                    var select = document.createElement('select');
                    select.setAttribute('name', columnOption.editOption.name);
                    for (var i = 0; i < columnOption.editOption.option().length; i++) {
                        select.options.add(columnOption.editOption.option()[i]);
                    }
                    /*
                    if (columnOption.editOption.hasOwnProperty('isView')) {
                        select.addEventListener('blur', function () {
                            // 기본 기능
                            $(this).prev().text($(this).find(' option:selected').text());
                            $(this).remove();
                            span.show();
    
                            // 확장 기능
                            // 서버 저장 같은..
                        });
                    }
                    */
                    tdTag.appendChild(select);
                    select.focus();

                } else if ('text') {
                    var txtInput = document.createElement('input');
                    txtInput.setAttribute('type', 'text')
                    txtInput.setAttribute('style', 'width:80%;');
                    txtInput.setAttribute('value', span.text());
                    txtInput.setAttribute('name', columnOption.editOption.name);
                    tdTag.appendChild(txtInput);
                    txtInput.focus();
                }
            } else {
                var opts = new Array();
                for (var i = 0; i < columnOption.editOption.options.length; i++) {
                    var txtInput = document.createElement('input');
                    txtInput.setAttribute('type', 'text')
                    txtInput.setAttribute('style', 'width:40%;');
                    txtInput.setAttribute('name', columnOption.editOption.options[i].name);
                    //txtInput.setAttribute('value', value[i]);

                    //span.style.display = 'none';
                    opts.push(txtInput);
                    //tdTag.appendChild(txtInput);
                }
                var opt = columnOption.editOption.formatter(opts, value);
                console.log(opt);
                $(tdTag).append(opt);
                //tdTag.appendChild(opt);
                //return tdTag;
            }

        }

        if (columnOption.hasOwnProperty('formatter') && !columnOption.hasOwnProperty('editOption')) {
            val = columnOption.formatter(value);
            var span = document.createElement('span');
            $(span).append(val);
            tdTag.appendChild(span);
        }
        if (columnOption.hasOwnProperty('align')) {
            tdTag.style.textAlign = columnOption.align;
        }
        //span.after('<input type=text style="width:100%;" onblur="alert(123);" value="'+span.text()+ '" />');
        return tdTag;
    }

    /**
     * setCellData
     * 실제 Cell(td)에 들어갈 데이터를 제작한다.
     * @param {text} value 
     * @param {ColumnConfig} columnOption 
     */
    function setCellData(value, columnOption) {

        //cellTypeHtml = '<input type="checkbox">';
        var tdTag = document.createElement('td');
        var val = value;

        if (columnOption.hasOwnProperty('formatter')) {
            val = columnOption.formatter(value);
        }
        if (columnOption.hasOwnProperty('align')) {
            tdTag.style.textAlign = columnOption.align;
        }

        var span = document.createElement('span');
        //span.innerHTML = val;
        $(span).append(val);
        //tdTag.innerHTML = val;
        if (columnOption.hasOwnProperty('inputOption')) {
            var input = document.createElement('input');
            var keys = Object.keys(columnOption.inputOption);
            for (var i = 0; i < keys.length; i++) {
                if (keys[i] == 'event') {
                    var eventKeys = Object.keys(columnOption.inputOption.event);
                    for (var j = 0; j < eventKeys.length; j++) {
                        var evt = columnOption.inputOption.event[eventKeys[j]];
                        input.addEventListener(eventKeys[j], evt);
                    }
                } else {
                    input.setAttribute(keys[i], columnOption.inputOption[keys[i]] || value);
                }
            }
            tdTag.appendChild(input);
        }
        tdTag.appendChild(span);

        if (columnOption.hasOwnProperty('editOption')) {
            if (columnOption.editOption.hasOwnProperty('isView')) {
                // 옵션이 여러개 아니면..
                if (!columnOption.editOption.hasOwnProperty('options')) {
                    var editType = columnOption.editOption.type;
                    if (editType == 'selectbox') {
                        var select = document.createElement('select');
                        select.setAttribute('name', columnOption.editOption.name);
                        for (var i = 0; i < columnOption.editOption.option().length; i++) {
                            select.options.add(columnOption.editOption.option()[i]);
                        }
                        console.log(val);
                        select.value = val;
                        console.log(select.value)
                        span.style.display = 'none';
                        tdTag.appendChild(select);
                        return tdTag;

                    } else if ('text') {
                        var txtInput = document.createElement('input');
                        txtInput.setAttribute('type', 'text')
                        txtInput.setAttribute('style', 'width:80%;');
                        txtInput.setAttribute('value', val);
                        txtInput.setAttribute('name', columnOption.editOption.name);

                        span.style.display = 'none';
                        tdTag.appendChild(txtInput);
                        return tdTag;
                    }
                } else {
                    var opts = new Array();
                    for (var i = 0; i < columnOption.editOption.options.length; i++) {
                        var txtInput = document.createElement('input');
                        txtInput.setAttribute('type', 'text')
                        txtInput.setAttribute('style', 'width:40%;');
                        txtInput.setAttribute('value', value[i]);
                        txtInput.setAttribute('name', columnOption.editOption.options[i].name);

                        span.style.display = 'none';
                        opts.push(txtInput);
                        //tdTag.appendChild(txtInput);
                    }
                    var opt = columnOption.editOption.formatter(opts, value);
                    console.log(opt);
                    $(tdTag).append(opt);
                    //tdTag.appendChild(opt);
                    return tdTag;
                }

            }
        }

        tdTag.addEventListener('dblclick', function () {
            if (!columnOption.hasOwnProperty('editOption')) return false;
            console.log(this);
            var span = $(this).find('span');
            span.hide();

            var editType = columnOption.editOption.type;
            if (editType == 'selectbox') {
                var select = document.createElement('select');
                for (var i = 0; i < columnOption.editOption.option().length; i++) {
                    console.log(columnOption.editOption.option()[i]);
                    select.options.add(columnOption.editOption.option()[i]);
                }
                select.addEventListener('blur', function () {

                    // 기본 기능
                    $(this).prev().text($(this).find(' option:selected').text());
                    $(this).remove();
                    span.show();

                    // 확장 기능
                    // 서버 저장 같은..
                });
                tdTag.appendChild(select);
                select.focus();

            } else if ('text') {
                var txtInput = document.createElement('input');
                txtInput.setAttribute('type', 'text')
                txtInput.setAttribute('style', 'width:100%;');
                txtInput.setAttribute('value', span.text());
                txtInput.addEventListener('blur', function () {

                    // 기본 기능
                    $(this).prev().text($(this).val());
                    $(this).remove();
                    span.show();

                    // 확장 기능
                    // 서버 저장 같은..
                });
                tdTag.appendChild(txtInput);
                txtInput.focus();
            }


            //span.after('<input type=text style="width:100%;" onblur="alert(123);" value="'+span.text()+ '" />');
        });
        return tdTag;
    }

    /**
     * mergeHeader
     * Column의 Merge 여부를 확인하여 Merge 한다.
     * @param {Column} column 
     * @param {Headers} headerOpt 
     * @param {number} maxHeaderRowCnt 
     */
    function mergeHeader(column, headerOpt, maxHeaderRowCnt) {
        var maxColCnt = column.length;
        // 일반 컬럼과 머지 컬럼 모두 담는다.
        var notUsedColNms = setNotUsedColNms(column, headerOpt);

        var html = '';
        for (var j = 0; j < maxHeaderRowCnt; j++) {
            // 머지에 참여했던 컬럼은 건너뛴다.
            var mergedColNms = new Array();
            var usedMergeColNms = new Array();
            var testHtml = '';
            html += '<tr>'
            for (var i = 0; i < column.length; i++) {
                // 해당 컬럼의 col merge, row merge 여부 확인 및 merge cnt 조회
                var colNm = column[i].name;
                if (mergedColNms.indexOf(colNm) != -1) continue;
                var rowspanCnt = getRowspanCnt(headerOpt, colNm, maxHeaderRowCnt, notUsedColNms, j);
                var colMergeData = getColspanCnt(headerOpt, colNm, notUsedColNms);

                var colspanCnt = 1;
                if (colMergeData instanceof Object) {
                    colspanCnt = colMergeData.merged.length;
                    colNm = colMergeData.name;
                    mergedColNms = colMergeData.merged;
                }

                if (notUsedColNms.indexOf(colNm) == -1) continue;
                if (colspanCnt > 1) {
                    // 1. merge 할 여지가 있으면 머지를 생선한다 한다. 뭐지?
                    // 2. merge 할 여지가 있지만 merge 하지 않아야할 컬럼 체크
                    html += '<th col-name="' + colNm + '" colspan="' + colspanCnt + '" rowspan="' + rowspanCnt + '">' + colMergeData.title + '</th>';
                    testHtml = '<th col-name="' + colNm + '" colspan="' + colspanCnt + '" rowspan="' + rowspanCnt + '">' + colMergeData.title + '</th>';
                } else {
                    html += '<th col-name="' + colNm + '" colspan="' + colspanCnt + '" rowspan="' + rowspanCnt + '">' + column[i].title + '</th>';
                    testHtml = '<th col-name="' + colNm + '" colspan="' + colspanCnt + '" rowspan="' + rowspanCnt + '">' + column[i].title + '</th>';
                }
                notUsedColNms.splice(notUsedColNms.indexOf(colNm), 1);
            }
            html += '</tr>'
        }
        return html;
    }

    /**
     * setNotUsedColNms
     * 각 컬럼이 그려졌는지 확인 하기 위한 컬럼 이름 배열 생성
     * @param {*} column 
     * @param {*} headerOpt 
     */
    function setNotUsedColNms(column, headerOpt) {
        // 일반 컬럼과 머지 컬럼 모두 담는다.
        var notUsedColNms = new Array();
        for (var i = 0; i < column.length; i++) {
            notUsedColNms.push(column[i].name);
        }

        for (var i = 0; i < headerOpt.merge.length; i++) {
            notUsedColNms.push(headerOpt.merge[i].name);
        }
        return notUsedColNms;
    }

    /**
     * Header에 사용 될 Row의 총 개수를 가져온다.
     * @param {GridConfig} gridConf 
     */
    function getMaxHeaderRowCnt(gridConf) {
        var header = gridConf.Header;
        var colList = gridConf.columnList;

        if (!header.hasOwnProperty('merge')) return 1;
        var rowCnt = 1;
        var maxRowCnt = 1;
        var mergeList = header.merge;

        for (var i = 0; i < colList.length; i++) {
            var nm = colList[i].name;
            for (var j = 0; j < mergeList.length; j++) {

                if (mergeList[j].merged.indexOf(nm) != -1) {
                    rowCnt += 1;
                }
            }

            if (maxRowCnt < rowCnt) {
                maxRowCnt = rowCnt;
            }
            rowCnt = 1;

        }
        return maxRowCnt;
    }

    /**
     * getRowspanCnt
     * 해당 컬럼의 rowspan Cnt를 리턴한다.
     * @param {*} headerOpt
     * @param {*} colNm 
     * @param {*} maxRowCnt 
     * @param {*} notUsedColNms 
     * @param {*} row 
     * @return rowspanCnt;
     */
    function getRowspanCnt(headerOpt, colNm, maxRowCnt, notUsedColNms, row) {
        var rowspanCnt = 1;
        if (!chkRowspan(headerOpt, colNm, notUsedColNms)) return maxRowCnt - (row - 1);
        var chkMerge = false;
        for (var i = 0; i < headerOpt.merge.length; i++) {
            if (notUsedColNms.indexOf(headerOpt.merge[i].name) != -1) {
                chkMerge = true;
            }
            if (headerOpt.merge[i].merged.indexOf(colNm) != -1) {
                chkMerge = true;
            }
        }
        if (!chkMerge) {
            // 더이상 합치고자시고할게없으면 아래로 다 합쳐버린다.
            rowspanCnt = maxRowCnt - (row - 1);
        }
        return rowspanCnt;
    }

    /**
     * chkRowspan
     * 해당 컬럼의 row 머지 여부 판단
     * @param {*} headerOpt
     * @param {*} colNm 
     * @param {*} usedColNms 
     */
    function chkRowspan(headerOpt, colNm, usedColNms) {
        for (var i = 0; i < headerOpt.merge.length; i++) {
            if (headerOpt.merge[i].merged.indexOf(colNm) != -1) {
                if (usedColNms.indexOf(headerOpt.merge[i].name) == -1) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 해당 컬럼의 colspan Cnt 리턴
     * @param {*} headerOpt 
     * @param {*} colNm 
     * @param {*} notUsedColNms 
     * @param {*} mergedColNms 
     * @return 
     */
    function getColspanCnt(headerOpt, colNm, notUsedColNms, mergedColNms) {
        var colspanCnt = 1;
        var tmpMergeObj = new Object();
        if (!chkColspan(headerOpt, colNm, notUsedColNms)) return colspanCnt;

        // notUsedColNms 에 merge 컬럼이 없으면 돌아간다.
        var chkUsed = false;
        for (var i = 0; i < headerOpt.merge.length; i++) {
            if (notUsedColNms.indexOf(headerOpt.merge[i].name) != -1) {
                chkUsed = true;
            }
        }
        if (!chkUsed) return colspanCnt;

        // merge 리스트에서 가장 큰 수의 merged 를 가져온다. 동일할 경우 우선 선언 된 것 먼저
        for (var i = 0; i < headerOpt.merge.length; i++) {
            if (notUsedColNms.indexOf(headerOpt.merge[i].name) == -1) continue;

            var tmpColsanCnt = 1;
            if (headerOpt.merge[i].merged.indexOf(colNm) != -1) {
                tmpColsanCnt = headerOpt.merge[i].merged.length;

            }
            if (colspanCnt <= tmpColsanCnt) {
                colspanCnt = tmpColsanCnt;
                tmpMergeObj = headerOpt.merge[i];
            }
        }

        return tmpMergeObj;
    }

    /**
     * 해당 컬럼의 col merge 여부 판단
     * @param {*} headerOpt 
     * @param {*} colNm 
     * @param {*} notUsedColNms 
     */
    function chkColspan(headerOpt, colNm, notUsedColNms) {
        for (var i = 0; i < headerOpt.merge.length; i++) {
            if (headerOpt.merge[i].merged.indexOf(colNm) != -1) {
                if (notUsedColNms.indexOf(headerOpt.merge[i].name) == -1) {
                    return false;
                } else {
                    return true;
                }
            }

            // 해당 컬럼이 속한 머지가 이미 생성되었다면 더이상 머지 할 필요가 없당.
        }
        return false;
    }

    /**
     * 데이터 row 병합
     * @param {*} column 
     */
    function dataRowspan(column) {
        //var tbl = $('[name=tbl_row]');
        var elId = gridConf.elementId
        var trs = $('[name=' + elId + '_tbl_general_row] tr, [name=' + elId + '_tbl_fixed_row] tr');
        var tmpStr = '';


        for (var i = 0; i < column.length; i++) {
            if (!column[i].hasOwnProperty('duplicateMerge')) continue;
            for (var j = 0; j < trs.length; j++) {
                var tr = trs[j];
                var tmpIdx = 1;
                if (column[i].hasOwnProperty('inputOption')) {
                    // input 옵션이 있을 시...
                    if ($($($(tr).find('td')[i]).find('input')).val() == $($($(trs[j + 1]).find('td')[i]).find('input')).val()) {
                        // 다음 tr의 td값과 같은가 ?
                        // 그렇다면 그 다음 tr의 td값과도 같을 수 있는 여지가 있군
                        // 다음 tr의 td 값과 동일한지 알아보자
                        for (var k = j + 1; k < trs.length; k++) {
                            if ($($($(tr).find('td')[i]).find('input')).val() == $($($(trs[k]).find('td')[i]).find('input')).val()) {
                                tmpIdx += 1;
                                $($(trs[k]).find('td')[i]).hide();
                            } else {
                                break;
                            }
                        }
                        $($(tr).find('td')[i]).attr('rowspan', tmpIdx);
                    }
                } else {
                    if ($($(tr).find('td')[i]).text() == $($(trs[j + 1]).find('td')[i]).text()) {
                        // 다음 tr의 td값과 같은가 ?
                        // 그렇다면 그 다음 tr의 td값과도 같을 수 있는 여지가 있군
                        // 다음 tr의 td 값과 동일한지 알아보자
                        for (var k = j + 1; k < trs.length; k++) {
                            if ($($(tr).find('td')[i]).text() == $($(trs[k]).find('td')[i]).text()) {
                                console.log($($(tr).find('td')[i]).text());
                                tmpIdx += 1;
                                $($(trs[k]).find('td')[i]).hide();
                            } else {
                                break;
                            }
                        }
                        console.log(tmpIdx);
                        $($(tr).find('td')[i]).attr('rowspan', tmpIdx);
                    }
                }
            }
        }
    }

    /**
     * 데이터 row 병합 해제
     */
    function dataRowspanCancel() {
        // 각각에 대해서 확인하고 진행하여도 되지만 일단은 그냥 한방에 해결한다.
        $('[name=tbl_row] td').removeAttr('rowspan').css('display', 'table-cell');
    }


    /**
     * Row를 추가한다.
     */
    function addRow(text) {
        console.log(text);
        // 1. 추가해야할 컬럼 정보를 가져온다.
        var column = colConf.generalColumn;
        var divNm = gridConf.elementId;

        // 2. fixed, general 구분하여 그린다
        // 노출할 데이터가 없기때문에 edit옵션이 있는 경우에만 input box를 그려준다.

        // 미리 row를 만들어 놓으면 좋을 것 같다. 
        var row = new Object();

        var tr = document.createElement('tr');
        tr.setAttribute('data_row', Number($('[name=' + divNm + '_tbl_general_row] tr').last().attr('data_row')) + 1);
        for (var j = 0; j < column.length; j++) {
            var cellTypeHtml = '';
            var dataValue = '';
            var td = setCellInput(dataValue, column[j]);
            var className = ($($('[name=' + divNm + '_tbl_general_row] tr').last().find('td')[0]).attr('class') == 'row2') ? 'row1' : 'row2';
            td.className = className;
            tr.append(td);
        }
        $('[name=' + divNm + '_tbl_general_row').append(tr);
    }

    function removeRow(idx) {
        $('tr[data_row="' + idx + '"]').remove();
        console.log(idx);
    }

    function getRowIndex(el) {
        var idx = 0;
        if ($(el).parents('tr').length != 0) {
            idx = $(el).parents('tr').attr('data_row');
        }
        return idx;
    }

    function getRowData(idx) {
        var tr = $('tr[data_row="' + idx + '"]');
        // td내에서 모든 input 항목을 가져와서 객체로 만든다.
        var arr = new Array();
        var map = new Map();
        tr.find('td input[type=text], td input[type=checkbox], td input[type=radio], td select').each(function () {
            map[$(this).attr('name')] = $(this).val();
        });
        return map;
    }

    function getAllRowData() {
        var trs = $('tr[data_row]');
        // td내에서 모든 input 항목을 가져와서 객체로 만든다.
        var arr = new Array();

        trs.each(function () {
            console.log($(this).attr('data_row'));
            var a = $(this).attr('data_row');
            var map = new Map();
            $(this).find('td input[type=text], td input[type=checkbox], td input[type=radio], td select').each(function (a) {
                map.set($(this).attr('name'), $(this).val());
                console.log(a + '_' + $(this).attr('name') + '_' + $(this).val());
            });
            console.log('map size = ' + map.size);
            if (map.size != 0) arr.push(map);
        });
        console.log(arr);
        return arr;
    }

    function testM() {
        alert('abc');
    }

























    return Grid;
})();