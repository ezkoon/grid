# Grid 라이브러리

Grid 라이브러리 설계

## 준비

    1. 라이브러리 목적
    2. 라이브러리 사용 방법 설계 
        a. DHTMLX
        b. Kendo UI
        c. Toast UI
    3. 라이브러리 구조 설계

## 1. 라이브러리 목적
테이블 형식의 데이터 출력을 쉽게 개발하기 위함.

## 2. 라이브러리 사용 방법 설계
UI 라이브러리를 제공하는 서비스 중 Grid 사용 방법에 대해 알아본다.
### a. DHTMLX
> 특징 : 자바 Excel 라이브러리 POI와 비슷한 방식
>
> 장점 : 무엇을 하고자 하는지 명확함. 어떤 행위에 대하여 직관적.
>
> 단점 : 출력 형식에 제한은 없으나 출력 형식이 복잡해질 경우 코드도 복잡해짐.

#### - 초기화 방법
    var myGrid;
    function doOnLoad(){
        myGrid = new dhtmlXGridObject('gridbox');
        myGrid.setImagePath("../../../codebase/imgs/");
        myGrid.setHeader("Sales,edtxt,ed,Price,In Store,Shipping,Bestseller,Date of Publication");
        myGrid.setInitWidths("70,150,100,80,80,80,80,100");
        myGrid.setColAlign("right,left,left,right,center,left,center,center");
        myGrid.setColTypes("dyn,edtxt,ed,price,ch,co,ra,ro");
        myGrid.setColSorting("int,str,str,int,str,str,str,date");
        myGrid.enableAutoWidth(true);
        myGrid.enableAutoHeight(true);
        myGrid.init();
        myGrid.enableAlterCss("even","uneven");
        myGrid.load("../common/grid.xml");  
    }
----

### b. Kendo UI
> 특징 : 데이터 바인딩 방식을 참고할만 함.
>
> 장점 : 구조가 명확하여 유지보수 쉬움.
>
> 단점 : Jquery 의존

#### - 초기화 방법
     $("#grid").kendoGrid({
                dataSource: {
                    type: "odata",
                    transport: {
                        read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Orders"
                    },
                    schema: {
                        model: {
                            fields: {
                                OrderID: { type: "number" },
                                ShipCountry: { type: "string" },
                                ShipName: { type: "string" },
                                ShipCity: { type: "string" },
                                ShipAddress: { type: "string" }
                            }
                        }
                    },
                    pageSize: 30
                },
                height: 540,
                sortable: true,
                reorderable: true,
                groupable: true,
                resizable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                columns: [ {
                        field: "OrderID",
                        title: "Order ID",
                        locked: true,
                        lockable: false,
                        width: 150
                    }, {
                        field: "ShipCountry",
                        ...
                        ...
---

### c. Toast UI
> 특징 : hearder merge가 쉬움.
>
> 장점 : Kendo UI와 동일. 좀 더 직관적
>
> 단점 : Jquery 의존

#### - 초기화 방법
    var grid = new tui.Grid({
        el: $('#grid'),
        selectType: 'checkbox',
        columnFixIndex: 5,
        displayRowCount: 10,
        headerHeight: 135,
        minimumColumnWidth: 20,
        columnMerge : [
            {
                columnName : "mergeColumn1",
                title : "배송방식+상품주문번호",
                columnNameList : ["deliveryType", "productOrderNo"]
            },
            {
                columnName : "mergeColumn2",
                title : "구매자명+구매자ID",
                columnNameList : ["orderName", "orderId"]
            },
            {
                columnName : "mergeColumn3",
                title : "구매자명+구매자ID+수취인명",
                columnNameList : ["mergeColumn2", "addressee"]
            }
           
        ],
        columnModelList : [
            {
                "title" : "<b>날짜</b>",
                "columnName" : "date1",
                "width" : 100,
                "editOption" : {
                    type: 'text'
                }
            },
            {
                "title" : "<b>배송방식</b>",
                "columnName" : "deliveryType",
                "width" : 100,
                "editOption" : {
                    type: 'text',
                    maxLength : 10,
                    "changeBeforeCallback" : function(changeEvent){
                        console.log("배송 callback", changeEvent)
                    }
                }
            },
            ...
            ...
---
## 3. 라이브러리 구조 설계
* 설계의 목표 
> 1. 사용 방법이 쉬워야 한다.
> 2. 확장이 용이해야 한다.

#### - 초기화 방법
     var grid = new Grid({
        id: 'elementId',
        width : 1000,
        height : 500
        useScrollBar: true,
        resizeColWidth : false,


        columnList: [
                { name: "col_1", title: "날짜", width: 120 
                    ,option :{
                        type        : 'checkbox',
                        fixColumn   : true,
                        textAlign   : 'center',
                        formatter   : function(){
                                return '<b>' + value + '</b>';
                        }
                    }, event : {
                        mouseover   : changeBgColor('red'),
                        click       : alert('aaa');
                    }

                },
                { name: "col_2", title: "공동주택", width: 120 },
                { name: "col_3", title: "학교", width: 120 },
                { name: "col_4", title: "합계", width: 120 }
                ...


> 헤더 선언 방식 1
>
> merge를 쉽게 할 수 있을 것 같으나 실용적이지 못 함.
     
     /*   
        2 : [[0],[1,   2,   3]],
        1 : [    [1,   2], [3]],
        0 : [    [1], [2]     ]
    */
           
    headerMerge: [
            {
                rowIdx: 2,
                colMerge: [
                    { title: "날짜", col: [10] },
                    { title: "선거정보", col: [1, 2, 3] },
                    { title: "평균", col: [4] }
                ]
            },
            {
                rowIdx: 1,
                colMerge: [
                    { title: "구분", col: [1, 2] },
                    { title: "합계", col: [3] }
                ]
            },
            {
                rowIdx: 0,
                colMerge: [
                    { title: "공동주택", col: [1] },
                    { title: "학교", col: [2] }
                ]
            },
        ]

> 헤더 선언 방식 2
>
> Toast UI 와 비슷한 방식으로 직관적

    headerMerge: [
                    { name: "merge_1", title: "구분", merged: ["col_2", "col_3"] },
                    { name: "merge_2", title: "선거정보", merged: ["col_2", "col_3", "col_4"] }
                ]


# 결론
>1. 사용방법 설계
>> DHTMLX 외의 Grid는 대부분 비슷하며 기능들이 대동소이함. Toast UI 와 동일한 방식으로 진행하고자 함
>
>2. 확장성
>> 대부분의 Grid에서 확장에 대한 개념은 없는 것으로 보이며, 굳이 찾고자하면 툴바, 페이징 등의 확장 정도만 있다.
>>
>> 확장의 예 중 하나인 매트릭스의 경우는 피벗, 열고정 등의 방법으로 사용하고 있음.
>>
>> 우리 라이브러리에서 확장성을 적용하고자 하면 우선 확장 가능성의 한계가 있어야 하며, 
>>
>> 역으로 Grid 의 최소 단위를 Table이 아닌 Cell로 시작할 경우 단순 데이터 출력이 아닌 여러 형식(입력 폼 - 이용신청서 양식, 후보자 관리 등)의 표현도 가능 할 것 같음.

## 참고 사이트
* DHTMLX
>[https://docs.dhtmlx.com/grid__basic_initialization.html](https://docs.dhtmlx.com/grid__basic_initialization.html)
* Kendo UI
>[http://demos.telerik.com/kendo-ui/grid/index](http://demos.telerik.com/kendo-ui/grid/index)
* Toast UI
>[https://github.com/nhnent/tui.grid/tree/master/examples](https://github.com/nhnent/tui.grid/tree/master/examples)